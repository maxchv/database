create database lesson01
go
use lesson01
go
create table parent_table
(
	EmpNum int primary key identity(100, 1),
	FirstName nvarchar(50) not null check(FirstName != N''),
	LastName nvarchar(50) not null check(LastName != N''),
	DeptNum int
)
go
create table child_table
(
	SerialNum primary key
)
