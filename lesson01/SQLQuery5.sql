create database Academy
go
use Academy
go
create table [Teachers]
(
	[Id] int primary key identity not null,
	[EmploymentDate] date check(EmploymentDate > '01.01.1990') not null,
	[Name] nvarchar(max) check([Name] != '') not null,
	[Premium] money check(Premium >= 0) default 0 not null,
	[Salary] money check(Salary > 0) not null,
	[Surname] nvarchar(max) check([Surname] != '') not null
	
)
create table [Groups]
(
	[Id] int primary key identity not null,
	[Name] nvarchar(10) check([Name] != '') unique not null,
	[Rating] int check(Rating between 0 and 5) not null,
	[Year] int check([Year] between 1 and 5) not null
)
create table [Faculties]
(
	[Id] int primary key identity not null,
	[Name] nvarchar(100) unique check([Name] != '') not null,
)
create table [Departments]
(
	[Id] int primary key identity not null,
	[Financing] money check(Financing >= 0) default '0' not null,
	[Name] nvarchar(100) check([Name] != '') unique not null,
)
go