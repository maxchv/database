create table [Departments]
(
	[Id] int not null identity(1, 1) primary key,
	[Building] int not null check ([Building] between 1 and 5),
	[Financing] money not null check ([Financing] >= 0.0) default 0.0,
	[Floor] int not null check ([Floor] >= 1),
	[Name] nvarchar(100) not null unique check ([Name] <> N'')
);
go

create table [Diseases]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(100) not null unique check ([Name] <> N''),
	[Severity] int not null check ([Severity] >= 1) default 1
);
go

create table [Doctors]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(50) not null check ([Name] <> N''),
	[Phone] char(10) null,
	[Premium] money not null check ([Premium] >= 0.0) default 0.0,
	[Salary] money not null check ([Salary] > 0.0),
	[Surname] nvarchar(50) not null check ([Surname] <> N'')
);
go

create table [Examinations]
(
	[Id] int not null identity(1, 1) primary key,
	[DayOfWeek] int not null check ([DayOfWeek] between 1 and 7),
	[EndTime] time not null,
	[Name] nvarchar(100) not null unique check([Name] <> N''),
	[StartTime] time not null check ([StartTime] between '08:00' and '18:00'),
	check ([StartTime] < [EndTime])
);
go

create table [Wards]
(
	[Id] int not null identity(1, 1) primary key,
	[Building] int not null check ([Building] between 1 and 5),
	[Floor] int not null check ([Floor] >= 1),
	[Name] nvarchar(20) not null unique check([Name] <> N'')
);
go

--------------------
insert into Departments(Building, Financing,[Floor],[Name]) values ('10','70000','2',N'����')
insert into Departments(Building, Financing,[Floor],[Name]) values ('1','10000','3',N'����������')
insert into Departments(Building, Financing,[Floor],[Name]) values ('2','12500','3',N'������')
insert into Departments(Building, Financing,[Floor],[Name]) values ('3','20000','5',N'����������� ')
insert into Departments(Building, Financing,[Floor],[Name]) values ('4','35000','1',N'��������')
insert into Departments(Building, Financing,[Floor],[Name]) values ('5','15000','7',N'���������� ')
go
-------------------
insert into Diseases ([Name], Severity) values (N'�������','3')
insert into Diseases ([Name], Severity) values (N'�������','4')
insert into Diseases ([Name], Severity) values (N'���������','1')
insert into Diseases ([Name], Severity) values (N'������','1')
insert into Diseases ([Name], Severity) values (N'����','3')
go
-------------------
insert into Doctors([Name], Phone, Premium, Salary, Surname) values (N'�����', '0936717174','70','10000',N'��������')
insert into Doctors([Name], Phone, Premium, Salary, Surname) values (N'�������', '0638746173','50','15000',N'������������')
insert into Doctors([Name], Phone, Premium, Salary, Surname) values (N'����', '0939856186','100','12500',N'�����������')
insert into Doctors([Name], Phone, Premium, Salary, Surname) values (N'Nikita', '0638965163','200','13500',N'������')
insert into Doctors([Name], Phone, Premium, Salary, Surname) values (N'���������', '0637876183','150','17000',N'��������')
go
--------------------
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('2','19:00',N'����������','8:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('3','15:00',N'�������������','10:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('1','11:00',N'�������� ��������','9:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('4','10:00',N'�������� ������ � �����','9:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('5','16:00',N'����� ������� ��������� ��������','8:00')
go
---------------------
insert into Wards(Building, [Floor], [Name]) values ('4','1','�������')
insert into Wards(Building, [Floor], [Name]) values ('1','2','���')
insert into Wards(Building, [Floor], [Name]) values ('5','1','������')
insert into Wards(Building, [Floor], [Name]) values ('2','2','�������')
insert into Wards(Building, [Floor], [Name]) values ('3','1','������������')
go