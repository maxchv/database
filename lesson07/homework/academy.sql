use [master];
go

if db_id('Academy') is not null
begin
	drop database [Academy];
end
go

create database [Academy];
go

use [Academy];
go

create table [Departments]
(
	[Id] int not null identity(1, 1) primary key,
	[Financing] money not null check ([Financing] >= 0.0),
	[Name] nvarchar(100) not null unique check ([Name] <> N'')
);
go

create table [Faculties]
(
	[Id] int not null identity(1, 1) primary key,
	[Dean] nvarchar(100) not null check ([Dean] <> N''),
	[Name] nvarchar(100) not null unique check ([Name] <> N''),
);
go

create table [Groups]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(10) not null unique check ([Name] <> N''),
	[Rating] float not null check ([Rating] between 0 and 5),
	[Year] int not null check ([Year] between 1 and 5)
);
go

create table [Teachers]
(
	[Id] int not null identity(1, 1) primary key,
	[EmploymentDate] date not null check ([EmploymentDate] >= '1990-01-01'),
	[IsAssistant] bit not null default 0,
	[IsProfessor] bit not null default 0,
	[Name] nvarchar(50) not null check ([Name] <> N''),
	[Position] nvarchar(100) not null check ([Position] <> N''),
	[Premium] money not null check ([Premium] >= 0.0) default 0.0,
	[Salary] money not null check ([Salary] > 0.0),
	[Surname] nvarchar(50) not null check ([Surname] <> N'')
);
go
-------------------
insert into Departments (Financing, [Name]) values ('10000', N'Економики')
insert into Departments (Financing, [Name]) values ('15000', N'Финансов')
insert into Departments (Financing, [Name]) values ('5000', N'Анатомии')
insert into Departments (Financing, [Name]) values ('20000', N'Психологии')
insert into Departments (Financing, [Name]) values ('25000', N'Физиологии')
-------------------
insert into Faculties (Dean, [Name]) values (N'Юрий Зинченко',N'Факультет Психологии')
insert into Faculties (Dean, [Name]) values (N'Евгений Салыгин',N'Факультет права')
insert into Faculties (Dean, [Name]) values (N'Гамзат Магомедбеков',N'Факультет управления')
insert into Faculties (Dean, [Name]) values (N'Андрей Сидоров',N'Факультет Мировой Политики')
insert into Faculties (Dean, [Name]) values (N'Константин Солянник  ',N'Факультет адвокатуры')
---------------------
insert into Groups ([Name], Rating, [Year]) values (N'EKO 17-A-1','5','5')
insert into Groups ([Name], Rating, [Year]) values (N'EKO 18-A-1','4','4')
insert into Groups ([Name], Rating, [Year]) values (N'EKO 19-A-1','3','3')
insert into Groups ([Name], Rating, [Year]) values (N'EKO 20-A-1','2','2')
insert into Groups ([Name], Rating, [Year]) values (N'EKO 21-A-1','1','1')
-------------------------
insert into Teachers (EmploymentDate, IsAssistant, IsProfessor,[Name], Position,Premium,Salary,Surname) values ('2021-02-21', '1','0',N'Мария',N'Маклер','1500','10000',N'Жовта')
insert into Teachers (EmploymentDate, IsAssistant, IsProfessor,[Name], Position,Premium,Salary,Surname) values ('2019-12-27', '0','1',N'Виталий',N'Фінансовий менеджер','2500','50000',N'Тыльный')
insert into Teachers (EmploymentDate, IsAssistant, IsProfessor,[Name], Position,Premium,Salary,Surname) values ('2018-02-1', '0','1',N'Игорь',N'Фінансовий директор','5000','75000',N'Кровавый')
insert into Teachers (EmploymentDate, IsAssistant, IsProfessor,[Name], Position,Premium,Salary,Surname) values ('2020-03-15', '0','0',N'Егор',N'Фінансовий аналітик','3500','35000',N'Ликой')
insert into Teachers (EmploymentDate, IsAssistant, IsProfessor,[Name], Position,Premium,Salary,Surname) values ('2021-02-21', '1','0',N'Анастасия',N'Менеджер по роботі з клієнтами','1500','10000',N'Довговолосса')
-------------------