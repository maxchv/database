insert into Departments(Financing, Name)
	values (5000, 'Art'),
	       (15000, 'History'),
		   (20000, 'Philosophy'),
		   (25000, 'Business & Economics'),
		   (30000, 'Software Development')


insert into Faculties(Dean, Name)
	values ('Miguel Ford', 'Science'),
	       ('Samuel Adderiy', 'Technology'),
		   ('Jan Gonzalez', 'Business'),
		   ('Macon Martin', 'Computer Science'),
		   ('Gabriel Baker', 'Art & Social')


insert into Groups(Name, Rating, Year)
	values ('Group � 1', 1, 2),
	       ('Group � 2', 2, 3),
		   ('Group � 3', 3, 4),
		   ('Group � 4', 4, 5),
		   ('Group � 5', 5, 1)

insert into Teachers(EmploymentDate, IsAssistant, IsProfessor, Name, Position, Premium, Salary, Surname)
	values ('1997-09-01', 0, 1, 'John', 'Philosophy Teacher', 600, 1000, 'Lewis' ),
	       ('1999-01-01', 0, 1, 'Richard', 'Economics Teacher', 800, 1500, 'Nelson' ),
		   ('2006-04-01', 1, 0, 'Carl', 'Art Teacher', 150, 600, 'Johnson' ),
		   ('2010-03-01', 1, 0, 'Caleb', 'History Teacher', 400, 800, 'Dowman' ),
		   ('2013-05-01', 1, 0, 'Connor', 'Software Teacher', 500, 2000, 'Brooks' )

