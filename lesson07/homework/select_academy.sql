--1. ������� ������� ������, �� ����������� �� ���� � �������� �������.
select * from Departments
	order by Name desc
--2. ������� �������� ����� � �� �������� � ���������� ����
--����� ������ �������.
select Name as '��� ������', Rating as '������� ������' from Groups
--3. ������� ��� �������������� �� �������, ������� ������
--�� ��������� � �������� � ������� ������ �� ���������
--� �������� (����� ������ � ��������).
select Surname, Salary + Premium as '����� ������ � ��������' from Teachers 
--4. ������� ������� ����������� � ���� ������ ���� � ��������� �������: �The dean of faculty [faculty] is [dean].�.
select Dean, Name as 'The dean of faculty [faculty] is [dean].' from Faculties
--5. ������� ������� ��������������, ������� �������� ������������ � ������ ������� ��������� 1050.
select Surname from Teachers
	where IsProfessor=1 and Salary > 1050
--6. ������� �������� ������, ���� �������������� �������
--������ 11000 ��� ������ 25000.
select Name from Departments
	where Financing < 11000 or Financing > 25000
--7. ������� �������� ����������� ����� ���������� �Computer
--Science�.
select Name from Faculties
	where Name not like 'Computer Science'
--8. ������� ������� � ��������� ��������������, �������
--�� �������� ������������.
select Surname, Position from Teachers
	where IsProfessor=0
--9. ������� �������, ���������, ������ � �������� �����������, � ������� �������� � ��������� �� 160 �� 550.
select Surname, Position, Salary, Premium from Teachers
	where Premium between 160 and 550
--10.������� ������� � ������ �����������.
select Surname, Salary from Teachers
	where IsAssistant=1
--11.������� ������� � ��������� ��������������, �������
--���� ������� �� ������ �� 01.01.2000.
select Surname from Teachers 
	where EmploymentDate < '2000-01-01'
--12.������� �������� ������, ������� � ���������� �������
--������������� �� ������� �Software Development�. ��������� ���� ������ ����� �������� �Name of Department�.
select Name as 'Name of Department' from Departments
	where Name != 'Software Development'
	order by Name
--13.������� ������� �����������, ������� �������� (�����
--������ � ��������) �� ����� 1200.
select Surname from Teachers
	where IsAssistant=1 and Salary + Premium <= 1200
--14.������� �������� ����� 5-�� �����, ������� �������
--� ��������� �� 2 �� 4.
select Name from Groups
	where Rating between 2 and 4 and Year=5
--15.������� ������� ����������� �� ������� ������ 550 ���
--��������� ������ 200. 
select Surname from Teachers
	where IsAssistant=1 and Salary < 550 or Premium < 200
