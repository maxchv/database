-- Isolation � �� ����� ���������� ���������� ������������ ���������� �� ������ ��������� ������� �� � ���������.
-- ���������������� �������

USE acid
GO

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT 
	name '��������', 
	amount '�����'
FROM account;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

SELECT 
	name '��������', 
	amount '�����'
FROM account;

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;

SELECT 
	name '��������', 
	amount '�����'
FROM account;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SELECT 
	name '��������', 
	amount '�����'
FROM account;