-- Consistency - ����������, ����������� ������ ����������� ���������� �, ��� �����, 
-- ����������� ���� ����������, ��������� ��������������� ���� ������. 

USE acid
GO

TRUNCATE TABLE account
GO

INSERT INTO account(name, amount) 
	VALUES (N'����', 1000),
	       (N'����', 2000);

SELECT 
	name '��������', 
	amount '�����'
FROM account;

UPDATE account
SET amount =
	CASE name
		WHEN N'����' THEN amount - 1500
		WHEN N'����' THEN amount + 1500
	END
WHERE name IN (N'����', N'����');

SELECT 
	name '��������', 
	amount '�����'
FROM account;