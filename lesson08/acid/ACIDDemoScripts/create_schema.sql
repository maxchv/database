IF DB_ID('acid') IS NOT NULL
	DROP DATABASE acid;
GO
CREATE DATABASE acid;
GO
IF OBJECT_ID('acid.dbo.account') IS NOT NULL
	DROP TABLE acid.dbo.account;
GO
USE acid
GO
CREATE TABLE account
(
	id int primary key identity,
	name nvarchar(255) not null unique,
	amount money not null check(amount > 0),
	comment ntext
)
GO


