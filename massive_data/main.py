import urllib
import logging

from sqlalchemy.orm import sessionmaker, relationship, subqueryload
from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from random import choices

from faker import Faker
from faker_vehicle import VehicleProvider


logging.basicConfig(format='[%(asctime)s] %(message)s', datefmt='%d/%m/%Y %I:%M:%S', level=logging.INFO)

fake = Faker(locale='ru_RU')
fake.add_provider(VehicleProvider)

Base = declarative_base()
Session = sessionmaker()
PARAMS = urllib.parse.quote_plus('Driver={ODBC Driver 17 for SQL Server};Server=server1;Database=massive;UID=student')
CONNECTION_STRING = f"mssql+pyodbc:///?odbc_connect={PARAMS}"  # "sqlite:///database.db"
engine = create_engine(CONNECTION_STRING, echo=False)
Session.configure(bind=engine)


class Person(Base):
    __tablename__ = 'people'
    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    birthdate = Column(DateTime, nullable=False)
    cars = relationship('Car')

    def __init__(self, **kwargs):
        self.first_name = kwargs['first_name']
        self.last_name = kwargs['last_name']
        self.birthdate = kwargs['birthdate']

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'{self.id} {self.first_name} {self.last_name} {self.birthdate}'


class Car(Base):
    __tablename__ = "cars"
    id = Column(Integer, primary_key=True, autoincrement=True)
    make = Column(String(150), nullable=False)
    model = Column(String(150), nullable=False)
    category = Column(String(150), nullable=False)
    year = Column(Integer, nullable=False)
    person_id = Column(Integer, ForeignKey('people.id'), nullable=False)

    def __init__(self, **kwargs):
        self.make = kwargs['make']
        self.model = kwargs['model']
        self.year = kwargs['year']
        self.category = kwargs['category']

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f'{self.id} {self.make} {self.model} {self.category} {self.year}'


Base.metadata.create_all(engine)

session = Session()

MAX_COUNT = 40_000_000
COMMIT_EVERY = 1500

for i in range(MAX_COUNT):
    batch = []
    for _ in range(COMMIT_EVERY):
        p = Person(first_name=fake.first_name(), last_name=fake.last_name(), birthdate=fake.date_of_birth())
        n = choices(population=[0, 1, 2, 3, 4, 5, 6], weights=[25000, 10000, 5000, 500, 50, 10, 1])[0]
        for _ in range(n):
            c = Car(make=fake.vehicle_make(), model=fake.vehicle_model(), category=fake.vehicle_category(),
                    year=fake.vehicle_year())
            p.cars.append(c)
        batch.append(p)
    # session.add_all(batch)
    session.bulk_save_objects(batch)
    session.commit()
    logging.info(f"Commited {i*COMMIT_EVERY}")
session.commit()

# people = session.query(Person).join(Car).all()
# print(people[0])
