create database lesson01
go
use lesson01
go
create table parent_table
(
	EmpNum int primary key identity(100, 1),
	FirstName nvarchar(50) not null check(FirstName != N''),
	LastName nvarchar(50) not null check(LastName != N''),
	DeptNum int
)
go
create table child_table
(
	SerialNum int primary key,
	[Type] nvarchar(50) not null,
	EmpNum int not null default(100)
)
go
alter table child_table
add constraint child_table_parent_tabl_fk
	foreign key (EmpNum) references parent_table(EmpNum)
		on delete set default
		on update cascade
go
/*alter table child_table
drop constraint child_table_parent_tabl_fk
go*/

-- DML:
-- INSERT   - �������� ����� ������ � �������
-- UPDATE   - �������� ������
-- DELETE   - �������
-- TRUNCATE - ������� 

insert into parent_table(FirstName, LastName, DeptNum)
	              values(N'Jane', N'Smith', 101),
				        (N'Jim', N'Tate', 101),
				        (N'Ed', N'Rose', 102),
				        (N'Paul', N'Baker', 101)

update parent_table 
	set FirstName=N'����', LastName=N'������' 
		where EmpNum=1103 --FirstName = N'Ed'
-- � where ����� ������������ ���������:
-- =, >, <, !=, <>, between, like, in
-- and, or, not

delete parent_table
		where FirstName=N'����' and LastName=N'������'

truncate table child_table
go
truncate table parent_table
	