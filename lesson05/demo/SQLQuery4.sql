drop table if exists studsents
go
drop table if exists [group]
go


-- DDL (DATA DEFINITION LANGUAGE): CREATE, USE, DROP, ALTER
create table [group]
(
	[id] int primary key identity,
	[name] nvarchar(50) not null unique
)
go

create table students
(
	[id] int primary key identity,
	[first_name] nvarchar(30) not null,
	[last_name] nvarchar(30) not null,
	[group_id] int references [group](id)
)
go

-- DML (Data Manipulate Language): INSERT, UPDATE, DELETE
/*
	INSERT INTO <tbl_name>(<field1>, ...) VALUES (<value1>, ...)
*/
insert into [group](name) values(N'�� 20-3')
insert into [group](name) values(N'�� 19-1')

insert into students(first_name, last_name, group_id)
        values(N'����', N'������', 1)

insert into students(first_name, last_name, group_id)
        values(N'��������', N'�����', 3)