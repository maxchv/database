create database demo
go

use demo
go

CREATE TABLE students
(
	[id] [int] IDENTITY PRIMARY KEY,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[group_id] [int] NULL
)
GO

CREATE TABLE [social network](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[facebook] [nvarchar](50) NOT NULL,
	[student_id] [int] NULL UNIQUE
)
GO

ALTER TABLE [dbo].[social network]  
	ADD CONSTRAINT [social_network_students_fk] 
		FOREIGN KEY([student_id])
		REFERENCES [dbo].[students] ([id])
GO

create table groups
(
	id int primary key identity,
	name nvarchar(100) not null unique
)
go
alter table students
	add group_id int null,
	constraint students_group_fk
		foreign key(group_id) references groups(id)
go

/*
constraint students_group_fk
		foreign key(group_id) 
		references groups(id)
		ON DELETE NO ACTION|CASCADE|SET NULL|SET DEFAULT
		ON UPDATE NO ACTION|CASCADE|SET NULL|SET DEFAULT
*/
alter table students
add constraint default_group_id
	default 6 for group_id
go
alter table students
	drop constraint students_group_fk
go
alter table students
	add constraint students_group_fk
	foreign key(group_id) references groups(id)
		on delete set default
		on update cascade
go