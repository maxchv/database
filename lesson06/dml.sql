insert into Categories(CategoryName)
	values(N'Food'), (N'Drink');

update Categories
	set Description=N'����� ������� ���'
	where CategoryName=N'Food';

delete from Categories
	where CategoryID >= 10;

-- DDL ������� ��� �������� ������ ��
-- ������� � ������������ ������
-- truncate table Categories;

-- *.mdf, *.ldf, *.ndf

select * from Categories
select CategoryName, Description from Categories
select * from Categories where CategoryName=N'Seafood'
select CategoryName, Description from Categories where CategoryName=N'Seafood'

select * from Products 
	where UnitPrice between 10 and 100 -- UnitPrice >= 10 and UnitPrice <= 100

select * from Products
	where SupplierID in (1,3,5) --SupplierID = 1 or SupplierID = 3 or SupplierID = 5

/*
	% - ������������� � ����� ����������� �������� ����� ������
	_ - ������������� � ����� ��������� ��������
	[abc] - ������������� � a ��� b ��� c
	[^abc] - ������������� �� a �� b �� c
*/
select * from Products
	where ProductName like N'%D[_]a%'

