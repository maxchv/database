create database hospital
go
use hospital
go
create table [Departments]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(100) not null unique check ([Name] <> N'')
);
go

create table [Doctors]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(max) not null check ([Name] <> N''),
	[Premium] money not null check ([Premium] >= 0.0) default 0.0,
	[Salary] money not null check ([Salary] > 0.0),
	[Surname] nvarchar(max) not null check ([Surname] <> N'')
);
go

create table [DoctorsSpecializations]
(
	[Id] int not null identity(1, 1) primary key,
	[DoctorId] int not null,
	[SpecializationId] int not null
);
go

create table [Donations]
(
	[Id] int not null identity(1, 1) primary key,
	[Amount] money not null check ([Amount] > 0.0),
	[Date] date not null check ([Date] <= getdate()) default getdate(),
	[DepartmentId] int not null,
	[SponsorId] int not null
);
go

create table [Specializations]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(100) not null unique check ([Name] <> N'')
);
go

create table [Sponsors]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(100) not null unique check ([Name] <> N'')
);
go

create table [Vacations]
(
	[Id] int not null identity(1, 1) primary key,
	[EndDate] date not null,
	[StartDate] date not null,
	[DoctorId] int not null,
	check ([StartDate] < [EndDate])
);
go

create table [Wards]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(100) not null unique check([Name] <> N''),
	[DepartmentId] int not null
);
go

alter table [Donations]
add foreign key ([DepartmentId]) references [Departments]([Id]);
go

alter table [Donations]
add foreign key ([SponsorId]) references [Sponsors]([Id]);
go

alter table [DoctorsSpecializations]
add foreign key ([DoctorId]) references [Doctors]([Id]);
go

alter table [DoctorsSpecializations]
add foreign key (SpecializationId) references [Specializations]([Id]);
go

alter table [Vacations]
add foreign key ([DoctorId]) references [Doctors]([Id]);
go

alter table [Wards]
add foreign key ([DepartmentId]) references [Departments]([Id]);
go

-------------
insert into Doctors ([Name],Premium, Salary, Surname) values (N'Мария','150','1000', 'Высокогорная')
insert into Doctors ([Name],Premium, Salary, Surname) values (N'Виталий','200','1500', 'Тыльный')
insert into Doctors ([Name],Premium, Salary, Surname) values (N'Юрий','300','980', 'Довгоноссый')
insert into Doctors ([Name],Premium, Salary, Surname) values (N'Илья','250','2000', 'Товстолобик')
insert into Doctors ([Name],Premium, Salary, Surname) values (N'Егор','350','2500', 'Жовтый')
insert into Doctors ([Name],Premium, Salary, Surname) values (N'Сергей','100','5000', 'Куликов')
go
---------------
insert into Specializations ([Name]) values (N'Травматолог')
insert into Specializations ([Name]) values (N'Окулист')
insert into Specializations ([Name]) values (N'Хирург')
insert into Specializations ([Name]) values (N'Гинеколог')
insert into Specializations ([Name]) values (N'Уролог')
insert into Specializations ([Name]) values (N'Онколог')
insert into Specializations ([Name]) values (N'Стоматолог')
go
------------
insert into DoctorsSpecializations (DoctorId,SpecializationId) values ('1','1')
insert into DoctorsSpecializations (DoctorId,SpecializationId) values ('2','2')
insert into DoctorsSpecializations (DoctorId,SpecializationId) values ('3','3')
insert into DoctorsSpecializations (DoctorId,SpecializationId) values ('4','4')
insert into DoctorsSpecializations (DoctorId,SpecializationId) values ('5','5')
insert into DoctorsSpecializations (DoctorId,SpecializationId) values (6,6)
go
---------------
insert into Vacations (EndDate,StartDate,DoctorId) values ('2021.01.1','2020.11.01',1)
insert into Vacations (EndDate,StartDate,DoctorId) values ('2021.01.1','2020.10.01',2)
insert into Vacations (EndDate,StartDate,DoctorId) values ('2021.01.1','2020.11.01',3)
insert into Vacations (EndDate,StartDate,DoctorId) values ('2021.01.1','2020.09.01',4)
insert into Vacations (EndDate,StartDate,DoctorId) values ('2021.01.1','2020.08.01',5)
insert into Vacations (EndDate,StartDate,DoctorId) values (DATEADD(week, 3, getdate()),DATEADD(week, -1, getdate()),6)

go
-----------------
insert into Departments ([Name]) values (N'Операционное отделение')
insert into Departments ([Name]) values (N'Приёмное отделение')
insert into Departments ([Name]) values (N'Хирургическое')
insert into Departments ([Name]) values (N'Интенсивная терапия')
insert into Departments ([Name]) values (N'Урологическое')
go
----------------
insert into Wards ([Name], DepartmentId) values ('Палата №1','1')
insert into Wards ([Name], DepartmentId) values ('Палата №2','2')
insert into Wards ([Name], DepartmentId) values ('Палата №3','3')
insert into Wards ([Name], DepartmentId) values ('Палата №5','4')
insert into Wards ([Name], DepartmentId) values ('Палата №7','5')
go
-------------
insert into Sponsors ([Name]) values ('Umbrella Corporation')
insert into Sponsors ([Name]) values ('Helps everybody')
insert into Sponsors ([Name]) values ('All care')
insert into Sponsors ([Name]) values ('Helthy')
insert into Sponsors ([Name]) values ('HMD')
go
---------------
insert into Donations (Amount, [Date], DepartmentId, SponsorId) values('100000','2020-12-31','1','1')
insert into Donations (Amount, [Date], DepartmentId, SponsorId) values('250000','2021-02-20','2','2')
insert into Donations (Amount, [Date], DepartmentId, SponsorId) values('500000','2018-01-1','3','3')
insert into Donations (Amount, [Date], DepartmentId, SponsorId) values('700000','2019-11-1','4','4')
insert into Donations (Amount, [Date], DepartmentId, SponsorId) values('125945','2020-12-4','5','5')
go

create table [Diseases]
(
	[Id] int not null identity(1, 1) primary key,
	[Name] nvarchar(100) not null unique check ([Name] <> N''),
	[Severity] int not null check ([Severity] >= 1) default 1
);
go
create table [Examinations]
(
	[Id] int not null identity(1, 1) primary key,
	[DayOfWeek] int not null check ([DayOfWeek] between 1 and 7),
	[EndTime] time not null,
	[Name] nvarchar(100) not null unique check([Name] <> N''),
	[StartTime] time not null check ([StartTime] between '08:00' and '18:00'),
	check ([StartTime] < [EndTime])
);
go
insert into Diseases ([Name], Severity) values (N'Бронхит','3')
insert into Diseases ([Name], Severity) values (N'Инсульт','4')
insert into Diseases ([Name], Severity) values (N'Туберкулёз','1')
insert into Diseases ([Name], Severity) values (N'Цистит','1')
insert into Diseases ([Name], Severity) values (N'Корь','3')
go
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('2','19:00',N'Томография','8:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('3','15:00',N'Ренгенография','10:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('1','11:00',N'Контроль прививок','9:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('4','10:00',N'Проверка зрения и слуха','9:00')
insert into Examinations([DayOfWeek],EndTime,[Name],StartTime) values('5','16:00',N'Сдача полного комплекса анализов','8:00')
go

create table DoctorsExaminations
(
	DoctorID int not null references Doctors(Id),
	ExaminationID int not null references Examinations(Id)
)
go
alter table DoctorsExaminations
	add WardID int,
		foreign key(WardID) references Wards(ID)
go
insert into DoctorsExaminations(DoctorID, ExaminationID, WardID)
	values(1,1,2), (1,2,1), (1, 3, 2), (2, 4, 3), (3, 5, 2)
go
alter table Diseases
	add SpecializationId int,
	foreign key (SpecializationID) references Specializations(Id)
go

create table ExaminationsDiseases
(
	DiseaseId int not null references Diseases(Id),
	ExaminationId int not null references Examinations(Id)
)
go
insert into ExaminationsDiseases(DiseaseId, ExaminationId)
	values(1, 1), (2, 2), (3, 3), (4, 4), (5, 5)