create table Curators
(
Id int IDENTITY NOT NULL PRIMARY KEY,
[Name] nvarchar(max) NOT NULL check(Name != ''),
Surname nvarchar(max) NOT NULL check(Surname != '')
)
go
-------------
create table Faculties
(
Id int IDENTITY NOT NULL PRIMARY KEY,
Financing money NOT NULL check(Financing > 0) DEFAULT 0,
[Name] nvarchar(100) NOT NULL check (Name != '') unique
)
go
------------------
create table Departments
(
Id int IDENTITY NOT NULL PRIMARY KEY,
Financing money  NOT NULL check(Financing > 0) DEFAULT 0,
Name nvarchar(100) NOT NULL check(Name != '') unique,
FacultyId int NOT NULL
foreign key (FacultyId) references Faculties(Id)
)
go
--------------------
create table Groups
(
Id int IDENTITY NOT NULL PRIMARY KEY,
[Name] nvarchar(10) NOT NULL check(Name != '') unique,
[Year] int NOT NULL check([Year] BETWEEN 1 and 5),
DeparmentsId int NOT NULL,
foreign key (DeparmentsId) references Departments(Id)
)
go
---------------------
create table GroupsCurators
(
Id int IDENTITY NOT NULL PRIMARY KEY,
CuratorId int NOT NULL,
foreign key (CuratorId) references Curators(Id),
GroupId int NOT NULL,
foreign key (GroupId) references Groups(Id)
)
go
------------------
create table Subjects
(
Id int IDENTITY NOT NULL PRIMARY KEY,
[Name] nvarchar(100) NOT NULL check(Name != '') unique
)
go
----------------
create table Teachers
(
Id int IDENTITY NOT NULL PRIMARY KEY,
[Name] nvarchar(max) NOT NULL check(Name != ''),
Salary money NOT NULL check(Salary >= 0),
Surname nvarchar(max) NOT NULL check(Surname != '')
)
go
----------------
create table Lectures
(
Id int IDENTITY NOT NULL PRIMARY KEY,
LectureRoom nvarchar(max) NOT NULL check(LectureRoom != ''),
SubjectId int NOT NULL,
foreign key (SubjectId) references Subjects(Id),
TeacherId int NOT NULL,
foreign key (TeacherId) references Teachers(Id)
)
go
-------------------
create table GroupsLectures
(
Id int IDENTITY NOT NULL PRIMARY KEY,
GroupId int NOT NULL,
foreign key (GroupId) references Groups(Id),
LectureId int NOT NULL,
foreign key (LectureId) references Lectures(Id)
)
go

insert into Curators([Name], Surname) values (N'������',N'�������'),
(N'�����', N'�����'),
(N'����', N'׸����'),
(N'�������', N'���������'),
(N'���������', N'������')
go
insert into Faculties(Financing,[Name]) values ('10000',N'������������� ���������'),
('20000',N'������������� ���������'),
('25000',N'��������������� ���������'),
('6000',N'�������������� ���������'),
('1000',N'������������� ���������')
go
insert into Departments (Financing, [Name], FacultyId) values ('20000',N'������������� �������','1'),
('10000',N'������������� �������','2'),
('150000',N'��������������� �������','3'),
('5000',N'�������������� �������','4'),
('30000',N'������������� �������','5')
go
insert into Groups ([Name],[Year],DeparmentsId) values (N'���', '5','1'),
(N'��', '4','2'),
(N'��', '3','3'),
(N'��', '2','4'),
(N'��', '1','5')
go
insert into GroupsCurators (CuratorId, GroupId) values ('1','1'),
('2','2'),
('3','3'),
('4','4'),
('5','5')
go
insert into Subjects ([Name]) values (N'���������'),
(N'��������'),
(N'����������'),
(N'����������'),
(N'��������')
go
insert into Teachers ([Name], Salary, Surname) values (N'�����','10000',N'����������'),
(N'�����','15000',N'���������� '),
(N'������','5000',N'������'),
(N'����','20000',N'������'),
(N'������','40000',N'�����������')
go
insert into Lectures (LectureRoom, SubjectId, TeacherId) values (N'��������� ���������','1','1'),
(N'��������� ��������','2','2'),
(N'��������� ����������','3','3'),
(N'��������� ����������','4','4'),
(N'��������� ��������','5','5')
go
insert into GroupsLectures (GroupId, LectureId) values ('1','1'),
('2','2'),
('3','3'),
('4','4'),
('5','5')
go