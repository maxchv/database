Задание: Создать процедуру добавления фильма
При добавлении фильма в таблицу необходимо вставить DirectorID
Если в таблице Directors уже есть продюсер с указанными Именем и Фамилией,
то необходимо получить его DirectorID
Если нет, то создать новую запись в таблице Directors
Предусмотреть, что параметры @directorNationality и @directorBirth являются опциональными

Пример вызова процедуры:
exec sp_create_movie 
	@title=N'', @releaseYear=2001, @rating=5, @plot=N'', @movieLength=180, 
	@directorFirstName=N'', @directorLastName=N'', @directorNationality=N'', @directorBirth='1967-01-04'