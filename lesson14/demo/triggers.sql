print 'TRIGGERS'
/*
CREATE TRIGGER <trigger_namey> 
	ON <database>
	{AFTER|INSTEAD OF} [INSERT | UPDATE | DELETE]
AS 
BEGIN
   PRINT 'Your trigger'    
END
*/
print 'AFTER | INSTEAD OF'
select * from Address
insert into Address (Town, AddressType, NameAddress, Number)
values ('Dnipro', 'str', 'Yavornitskogo', 101), ('New York', 'str', 'Briton', 123);
go
-- ������� ������� ���� ����������
drop trigger if exists insert_address_trigger
go
-- ������� ������� 
create trigger insert_address_trigger
	on shop.dbo.address -- ��� ������� address
	after insert  -- ����� ������� ������
as
begin
	-- ��������� ����������
	declare @town nvarchar(100), @addresstype nvarchar(100), 
	        @nameaddress nvarchar(100), @Id int, @number nvarchar(10)
	-- ���������� ������ �� ����������� ������� inserted
	select @town = town, @addresstype=addresstype, @nameaddress=nameaddress,
	       @id=id, @number=number from inserted
	print cast(@id as varchar(10)) + ' ' + @town + ' ' + @addresstype + ' ' + @nameaddress + ' ' + @number
end
go

create trigger deny_delete_from_address
	on shop.dbo.address
	instead of delete
as
begin
	raiserror('Delete from address not permit', 10, 1)
end
go
delete from Address 
/*
    AFTER: ����������� ����� ���������� ��������. ������������ ������ ��� ������.

    INSTEAD OF: ����������� ������ �������� (�� ���� �� ���� �������� - ����������, 
	��������� ��� �������� - ������ �� �����������). 
	������������ ��� ������ � �������������
*/
print 'virtual tables INSERTED and DELETED'
/*
	��� ���������� (INSERT) ��� ��������� (UPDATE) ������ ����������� � 
	������������� ������� inserted. 
	��� ��������� �������������.

	��� �������� ��� ��������� ������ ���������� � ����������� ������� DELETED:
*/

/*
	������������ �������:
	� ���� ������ Movies �������� ������� Log � ������:
		- Id 
		- Action - �������� � ���� ������ (INSERT, UPDATE ��� DELETE)
		- Table  - �������� �������
		- When   - ����� �������
	
	� ������� Movie �������� �������� ��� ������������ ���������� DML ������.
	� ������ ������� ������ � ������� Log ���������� ������ � �������: Action='INSERT',
	Table='Movie', When=getdate()
	����������� ������ ������ ���������� ��� �������� � ���������� ������
*/