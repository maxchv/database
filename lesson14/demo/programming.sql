-- ���������� ����������
declare @personWithoutCar int = 0;
-- �������� �������� ���������� ����� ����� select � set
select @personWithoutCar=count(*) from people
	where not exists(select * from cars where cars.person_id = people.id)

select @personWithoutCar

set @personWithoutCar = 0

go

-- ������� IF
declare @personWithoutCar int = 10;
if @personWithoutCar = 0
	print 'It is zero'
else if @personWithoutCar = 10
	print 'It is ten'
else
	print 'Not zero and not ten'
-- �������: ������� �������� �������� ������
select MONTH(getdate())

go

-- ��������� CASE
declare @t int = 3;
declare @v nvarchar(25);
set @v = case @t
	when 1 then 'one'
	when 2 then 'two'
	when 3 then 'three'
end

select @v

set @v = case 
	when @t < 1 then 'less than one'
	when @t > 2 then 'greeater than two'
	else 'equal 2'
end
select @v

go

-- �����
declare @i int
declare @names nvarchar(1000) = ''
declare @name nvarchar(100)
select top 1 @i=id from people order by id
select @i
while @i < 10
begin
	select @name=first_name+' '+last_name from people where id=@i
	if @name = N'������ ��������'
	begin
		set @i = @i + 1
		continue	--break
	end
	set @names = @names + @name + ', '
	set @i = @i + 2
end
select @names
go
-- �������
create function month_name(@date date)
	returns nvarchar(15)
begin
	declare @m int;
	set @m = month(@date)
	return case @m
		when 1 then '������'
		when 2 then '�������'
		when 3 then '����'
		when 4 then '������'
		when 5 then '���'
		when 6 then '����'
		when 7 then '����'
		when 8 then '������'
		when 9 then '��������'
		when 10 then '������'
		when 11 then '������'
		when 12 then '�������'
	end
end
go
select dbo.month_name(getdate())

