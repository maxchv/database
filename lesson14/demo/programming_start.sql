-- ���������� ��������� ���������� � @@
/*
    @@CONNECTIONS
    @@ERROR  
    @@IDENTITY - 
    @@IDLE
    @@CPU_BUSY
    @@LANGUAGE
    @@ROWCOUNT  
    @@SERVERNAME 
    @@TOTAL_ERRORS
    @@VERSION  
	@@TRANCOUNT 
*/
USE [Academy]
GO

INSERT INTO [dbo].[Actors]
           ([FirstName]
           ,[LastName]
           ,[Nationality]
           ,[Birth])
     VALUES
           ('Vasja'
           ,'Pupkin'
           ,'Ukraine'
           ,'1989-01-01')
select @@IDENTITY
GO


select @@ERROR
go
PRINT 'Local Variables'
-- ��������� ����������: https://docs.microsoft.com/ru-ru/sql/t-sql/language-elements/variables-transact-sql?view=sql-server-ver15�

-- ���������� ��������� ����������
-- ������ �������� ����� ������ ���� ��������� ������ @
declare @count int = 0
select @count
print @count
set @count = @count + 1
print @count

go
-- https://docs.microsoft.com/ru-ru/sql/t-sql/functions/cast-and-convert-transact-sql?view=sql-server-ver15
-- https://docs.microsoft.com/ru-ru/sql/t-sql/data-types/media/lrdatahd.png?view=sql-server-ver15
declare @count int = 0
select @count = count(*) from Actors
print 'Actors = ' + cast(@count as varchar(5))

-- �������� �������� ���������� ����� ����� select � set


go
-- ���������� �����������

-- ������� IF...ELSE
PRINT 'IF...ELSE'
/*
https://docs.microsoft.com/ru-ru/sql/t-sql/language-elements/if-else-transact-sql?view=sql-server-ver15
IF Boolean_expression   
     { sql_statement | statement_block }   
[ ELSE IF  Boolean_expression
     { sql_statement | statement_block } ]
[ ELSE
     { sql_statement | statement_block } ]
*/
declare @vasja_id int
select @vasja_id = ActorID from Actors 
	where FirstName=N'����' and LastName = N'���������'
if @vasja_id is not null
begin
	print '������ ����! '
	print @vasja_id
end
else
	print '� ��� ����?'

-- �������: ������� �������� �������� ������
select MONTH(getdate())

-- ��������� END (BEGIN...END)
/*
BEGIN   
     { sql_statement | statement_block }   
END 
*/
go

-- ��������� CASE
PRINT 'CASE'
/*
https://docs.microsoft.com/ru-ru/sql/t-sql/language-elements/case-transact-sql?view=sql-server-ver15

--Simple CASE expression:   
CASE input_expression   
     WHEN when_expression THEN result_expression [ ...n ]   
     [ ELSE else_result_expression ]   
END   

--Searched CASE expression:  
CASE  
     WHEN Boolean_expression THEN result_expression [ ...n ]   
     [ ELSE else_result_expression ]   
END 
*/

declare @lang varchar(3) = 'RU'
print case @lang
		when 'RU' then '������'
		when 'EN' then 'Hello'
		else 'Hi'
	  end
go
declare @lang varchar(3) = 'EN'
print case 
		when @lang = 'RU' then '������'
		when @lang = 'EN' then 'Hello'
		else 'Hi'
	  end
go

use Northwind
go
select *, case 
	when UnitPrice > 50 then 10
	else 0
	end as 'Discount'
from Products

go
-- ����� WHILE 
PRINT 'WHILE'
/*
https://docs.microsoft.com/ru-ru/sql/t-sql/language-elements/while-transact-sql?view=sql-server-ver15

WHILE Boolean_expression   
     { sql_statement | statement_block | BREAK | CONTINUE } 
*/
declare @count int = 0
while @count < 10
begin
	print @count
	set @count = @count + 1
end

go

-- �������
PRINT 'FUNCTION'
/*
https://docs.microsoft.com/en-us/sql/t-sql/statements/create-function-transact-sql?view=sql-server-ver15

-- Transact-SQL Scalar Function Syntax
CREATE [ OR ALTER ] FUNCTION [ schema_name. ] function_name
( [ { @parameter_name [ AS ][ type_schema_name. ] parameter_data_type
 [ = default ] [ READONLY ] }
    [ ,...n ]
  ]
)
RETURNS return_data_type
    [ WITH <function_option> [ ,...n ] ]
    [ AS ]
    BEGIN
        function_body
        RETURN scalar_expression
    END
[ ; ]
*/
use demo
go
create or alter function dbo.m()
	returns int
begin
	declare @m int
	set @m = month(getdate())
	return @m
end
go

select dbo.m()

