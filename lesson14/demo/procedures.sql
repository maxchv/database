print 'STORE PROCEDURES'
/*
CREATE PROCEDURE <ProcedureName> 	
	<@Param1> <Datatype_For_Param1> = <Default_Value_For_Param1> [OUT], 
	<@Param2> <Datatype_For_Param2> = <Default_Value_For_Param2> [OUT]
AS
BEGIN
	-- BODY PROCEDURE HERE
END
GO
*/
go
-- ��������� ��� ����������
create procedure sp_unique_models
as
begin
	select distinct model from cars
end
go
print 'EXECUTE PROCEDURE'
-- �������� ���������
exec sp_unique_models
go
-- ��������� � ����� ������� ����������
create procedure sp_all_makers
	@count int -- ����� ������ ���� ���������� �����������
as
begin
	select make from cars group by make having count(make) >= @count
end
go

-- ����� ��������� � ����� ����������
exec sp_all_makers @count=10000

go

-- ��������� � �������� ����������
create procedure sp_create_car
	@make varchar(150),
	@model varchar(150),
	@category varchar(150),
	@year int,
	@person_id int,
	@id int out -- �������� ��������
as
begin
	-- ��������� ����������� �� � ������� cars ������������� @make
	if @make in (select distinct make from cars)
	begin
		print 'Make ' + @make + ' present in table cars'
		insert into cars(make, model, category, year, person_id)
			values(@make, @model, @category, @year, @person_id)
		set @id=@@IDENTITY
		print 'id = ' + cast(@id as varchar(10))
	end
	else
	begin
		print 'Make ' + @make + ' not present in table cars. Car will not insert'
		set @id = -1
	end
end
go

declare @id int
exec sp_create_car @make='BMW', @model='X5', @category='Van', @year=2010, @person_id=1, @id=@id out
print @id
select * from cars where id=@id

go
/*
�������: ������� ��������� ���������� ������
��� ���������� ������ � ������� ���������� �������� DirectorID
���� � ������� Directors ��� ���� �������� � ���������� ������ � ��������,
�� ���������� �������� ��� DirectorID
���� ���, �� ������� ����� ������ � ������� Directors
�������������, ��� ��������� @directorNationality � @directorBirth �������� �������������

������ ������ ���������:
exec sp_create_movie 
	@title=N'', @releaseYear=2001, @rating=5, @plot=N'', @movieLength=180, 
	@directorFirstName=N'', @directorLastName=N'', @directorNationality=N'', @directorBirth='1967-01-04'
*/
