print 'STORE PROCEDURES'
/*
CREATE PROCEDURE <ProcedureName> 	
	<@Param1> <Datatype_For_Param1> = <Default_Value_For_Param1> [OUT], 
	<@Param2> <Datatype_For_Param2> = <Default_Value_For_Param2> [OUT]
AS
BEGIN
	-- BODY PROCEDURE HERE
END
GO
*/
go
-- ��������� ��� ����������

print 'EXECUTE PROCEDURE'

go
-- ��������� � ����� ������� ����������

go

-- ����� ��������� � ����� ����������

go

-- ��������� � �������� ����������

go

-- ����� ��������� � �������� ����������

go
/*
�������: ������� ��������� ���������� ������
��� ���������� ������ � ������� ���������� �������� DirectorID
���� � ������� Directors ��� ���� �������� � ���������� ������ � ��������,
�� ���������� �������� ��� DirectorID
���� ���, �� ������� ����� ������ � ������� Directors
�������������, ��� ��������� @directorNationality � @directorBirth �������� �������������

������ ������ ���������:
exec sp_create_movie 
	@title=N'', @releaseYear=2001, @rating=5, @plot=N'', @movieLength=180, 
	@directorFirstName=N'', @directorLastName=N'', @directorNationality=N'', @directorBirth='1967-01-04'
*/
