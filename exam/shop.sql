use master
go
drop database if exists BookShop

go

create database BookShop
go
use BookShop

go

create table Shop
(
Id int IDENTITY NOT NULL PRIMARY KEY,
Name nvarchar(100) NOT NULL check(Name != '') unique
)

go
create table Countries
(
Id int IDENTITY NOT NULL PRIMARY KEY,
Name nvarchar(50) NOT NULL check(Name != '') unique
)

go
create table Shops
(
Id int IDENTITY NOT NULL PRIMARY KEY,
Name nvarchar(100) NOT NULL check(Name != ''),
ContryId int NOT NULL references Countries(Id)
)

go
create table Authors
(
Id int IDENTITY NOT NULL PRIMARY KEY,
Name nvarchar(100) NOT NULL check(Name != ''),
Surname nvarchar(100) NOT NULL check(Surname != ''),
ContryId int NOT NULL references Countries (Id)
)

go
create table Books
(
Id int IDENTITY NOT NULL PRIMARY KEY,
Name nvarchar(200) NOT NULL check(Name != ''),
Pages int NOT NULL check(Pages >=1),
Price money NOT NULL check(Price > 0),
PublishDate date not null check(PublishDate !> getdate()),
AuthorId int NOT NULL references Authors(Id),
ThemeId int NOT NULL references Themes(Id)
)

go
create table Sales
(
Id int primary key not null,
Price money not null check (Price > 0),
Quantity int not null check (Quantity >= 1),
SaleDate date NOT NULL check(SaleDate !> getdate()) DEFAULT getdate(),
BookId int NOT NULL references Books(Id),
ShopId int NOT NULL references Shops(Id)
)
go