USE [airport]
GO
ALTER TABLE [dbo].[Tickets] DROP CONSTRAINT [FK_Tickets_Passangers]
GO
ALTER TABLE [dbo].[Tickets] DROP CONSTRAINT [FK_Tickets_Flyghts]
GO
ALTER TABLE [dbo].[Tickets] DROP CONSTRAINT [FK_Tickets_Classes]
GO
ALTER TABLE [dbo].[PlanesPlasesCount] DROP CONSTRAINT [FK_PlanesPlasesCount_Planes]
GO
ALTER TABLE [dbo].[PlanesPlasesCount] DROP CONSTRAINT [FK_PlanesPlasesCount_Classes]
GO
ALTER TABLE [dbo].[Flyghts] DROP CONSTRAINT [FK_Flyghts_Planes]
GO
ALTER TABLE [dbo].[Flyghts] DROP CONSTRAINT [FK_Flyghts_Airports_To]
GO
ALTER TABLE [dbo].[Flyghts] DROP CONSTRAINT [FK_Flyghts_Airports_From]
GO
ALTER TABLE [dbo].[Airports] DROP CONSTRAINT [FK_Airports_Country]
GO
ALTER TABLE [dbo].[Airports] DROP CONSTRAINT [FK_Airports_City]
GO
/****** Object:  Index [IX_Country]    Script Date: 25.04.2021 21:35:19 ******/
DROP INDEX [IX_Country] ON [dbo].[Country]
GO
/****** Object:  Index [IX_City]    Script Date: 25.04.2021 21:35:19 ******/
DROP INDEX [IX_City] ON [dbo].[City]
GO
/****** Object:  Index [IX_Airports]    Script Date: 25.04.2021 21:35:19 ******/
DROP INDEX [IX_Airports] ON [dbo].[Airports]
GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tickets]') AND type in (N'U'))
DROP TABLE [dbo].[Tickets]
GO
/****** Object:  Table [dbo].[PlanesPlasesCount]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PlanesPlasesCount]') AND type in (N'U'))
DROP TABLE [dbo].[PlanesPlasesCount]
GO
/****** Object:  Table [dbo].[Planes]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Planes]') AND type in (N'U'))
DROP TABLE [dbo].[Planes]
GO
/****** Object:  Table [dbo].[Passangers]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Passangers]') AND type in (N'U'))
DROP TABLE [dbo].[Passangers]
GO
/****** Object:  Table [dbo].[Flyghts]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Flyghts]') AND type in (N'U'))
DROP TABLE [dbo].[Flyghts]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Country]') AND type in (N'U'))
DROP TABLE [dbo].[Country]
GO
/****** Object:  Table [dbo].[Classes]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Classes]') AND type in (N'U'))
DROP TABLE [dbo].[Classes]
GO
/****** Object:  Table [dbo].[City]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[City]') AND type in (N'U'))
DROP TABLE [dbo].[City]
GO
/****** Object:  Table [dbo].[Airports]    Script Date: 25.04.2021 21:35:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Airports]') AND type in (N'U'))
DROP TABLE [dbo].[Airports]
GO
USE [master]
GO
/****** Object:  Database [airport]    Script Date: 25.04.2021 21:35:19 ******/
DROP DATABASE [airport]
GO
/****** Object:  Database [airport]    Script Date: 25.04.2021 21:35:19 ******/
CREATE DATABASE [airport]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'airport', FILENAME = N'e:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER01\MSSQL\DATA\airport.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'airport_log', FILENAME = N'e:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER01\MSSQL\DATA\airport_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [airport] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [airport].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [airport] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [airport] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [airport] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [airport] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [airport] SET ARITHABORT OFF 
GO
ALTER DATABASE [airport] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [airport] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [airport] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [airport] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [airport] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [airport] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [airport] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [airport] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [airport] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [airport] SET  DISABLE_BROKER 
GO
ALTER DATABASE [airport] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [airport] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [airport] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [airport] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [airport] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [airport] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [airport] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [airport] SET RECOVERY FULL 
GO
ALTER DATABASE [airport] SET  MULTI_USER 
GO
ALTER DATABASE [airport] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [airport] SET DB_CHAINING OFF 
GO
ALTER DATABASE [airport] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [airport] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [airport] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'airport', N'ON'
GO
ALTER DATABASE [airport] SET QUERY_STORE = OFF
GO
USE [airport]
GO
/****** Object:  Table [dbo].[Airports]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airports](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[country_id] [int] NOT NULL,
	[city_id] [int] NOT NULL,
 CONSTRAINT [PK_Airports] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[City]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
 CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Classes]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Classes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Classes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Country]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Flyghts]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Flyghts](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[from] [int] NOT NULL,
	[to] [int] NOT NULL,
	[date] [datetime2](7) NOT NULL,
	[duration] [time](7) NOT NULL,
	[plane_id] [int] NOT NULL,
 CONSTRAINT [PK_Flyghts] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Passangers]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Passangers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[pasport] [nvarchar](50) NOT NULL,
	[birthdate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Passangers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Planes]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Planes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Planes] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlanesPlasesCount]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlanesPlasesCount](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[plane_id] [int] NOT NULL,
	[class_id] [int] NOT NULL,
	[count] [int] NOT NULL,
 CONSTRAINT [PK_PlanesPlasesCount] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tickets]    Script Date: 25.04.2021 21:35:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tickets](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[flyght_id] [int] NOT NULL,
	[passanger_id] [int] NOT NULL,
	[class_id] [int] NOT NULL,
	[payment_date] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_Tickets] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Airports] ON 

INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (1, N'Dnipro', 1, 1)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (2, N'Boryspil', 1, 2)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (3, N'Kyiv', 1, 2)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (4, N'Heathrow', 4, 5)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (5, N'City ', 4, 5)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (6, N'Gatwick ', 4, 5)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (7, N'Luton ', 4, 5)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (8, N'Stansted ', 4, 5)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (9, N'Southend ', 4, 5)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (10, N'Charles de Gaulle', 5, 6)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (11, N'Orly Airport ', 5, 6)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (12, N'Beauvais-Tillé Airport ', 5, 6)
INSERT [dbo].[Airports] ([id], [name], [country_id], [city_id]) VALUES (13, N'Paris–Le Bourget Airport ', 5, 6)
SET IDENTITY_INSERT [dbo].[Airports] OFF
GO
SET IDENTITY_INSERT [dbo].[City] ON 

INSERT [dbo].[City] ([id], [name]) VALUES (10, N'Berlin')
INSERT [dbo].[City] ([id], [name]) VALUES (1, N'Dnipro')
INSERT [dbo].[City] ([id], [name]) VALUES (2, N'Kiev')
INSERT [dbo].[City] ([id], [name]) VALUES (5, N'London')
INSERT [dbo].[City] ([id], [name]) VALUES (9, N'Montreal')
INSERT [dbo].[City] ([id], [name]) VALUES (4, N'New york')
INSERT [dbo].[City] ([id], [name]) VALUES (6, N'Paris')
INSERT [dbo].[City] ([id], [name]) VALUES (8, N'Vanucer')
INSERT [dbo].[City] ([id], [name]) VALUES (7, N'Washington')
INSERT [dbo].[City] ([id], [name]) VALUES (3, N'Zaporogie')
SET IDENTITY_INSERT [dbo].[City] OFF
GO
SET IDENTITY_INSERT [dbo].[Country] ON 

INSERT [dbo].[Country] ([id], [name]) VALUES (3, N'Canada')
INSERT [dbo].[Country] ([id], [name]) VALUES (5, N'France')
INSERT [dbo].[Country] ([id], [name]) VALUES (4, N'Great Britain')
INSERT [dbo].[Country] ([id], [name]) VALUES (1, N'Ukraine')
INSERT [dbo].[Country] ([id], [name]) VALUES (2, N'USA')
SET IDENTITY_INSERT [dbo].[Country] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Airports]    Script Date: 25.04.2021 21:35:19 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Airports] ON [dbo].[Airports]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_City]    Script Date: 25.04.2021 21:35:19 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_City] ON [dbo].[City]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Country]    Script Date: 25.04.2021 21:35:19 ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_Country] ON [dbo].[Country]
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Airports]  WITH CHECK ADD  CONSTRAINT [FK_Airports_City] FOREIGN KEY([city_id])
REFERENCES [dbo].[City] ([id])
GO
ALTER TABLE [dbo].[Airports] CHECK CONSTRAINT [FK_Airports_City]
GO
ALTER TABLE [dbo].[Airports]  WITH CHECK ADD  CONSTRAINT [FK_Airports_Country] FOREIGN KEY([country_id])
REFERENCES [dbo].[Country] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Airports] CHECK CONSTRAINT [FK_Airports_Country]
GO
ALTER TABLE [dbo].[Flyghts]  WITH CHECK ADD  CONSTRAINT [FK_Flyghts_Airports_From] FOREIGN KEY([from])
REFERENCES [dbo].[Airports] ([id])
GO
ALTER TABLE [dbo].[Flyghts] CHECK CONSTRAINT [FK_Flyghts_Airports_From]
GO
ALTER TABLE [dbo].[Flyghts]  WITH CHECK ADD  CONSTRAINT [FK_Flyghts_Airports_To] FOREIGN KEY([to])
REFERENCES [dbo].[Airports] ([id])
GO
ALTER TABLE [dbo].[Flyghts] CHECK CONSTRAINT [FK_Flyghts_Airports_To]
GO
ALTER TABLE [dbo].[Flyghts]  WITH CHECK ADD  CONSTRAINT [FK_Flyghts_Planes] FOREIGN KEY([plane_id])
REFERENCES [dbo].[Planes] ([id])
GO
ALTER TABLE [dbo].[Flyghts] CHECK CONSTRAINT [FK_Flyghts_Planes]
GO
ALTER TABLE [dbo].[PlanesPlasesCount]  WITH CHECK ADD  CONSTRAINT [FK_PlanesPlasesCount_Classes] FOREIGN KEY([class_id])
REFERENCES [dbo].[Classes] ([id])
GO
ALTER TABLE [dbo].[PlanesPlasesCount] CHECK CONSTRAINT [FK_PlanesPlasesCount_Classes]
GO
ALTER TABLE [dbo].[PlanesPlasesCount]  WITH CHECK ADD  CONSTRAINT [FK_PlanesPlasesCount_Planes] FOREIGN KEY([plane_id])
REFERENCES [dbo].[Planes] ([id])
GO
ALTER TABLE [dbo].[PlanesPlasesCount] CHECK CONSTRAINT [FK_PlanesPlasesCount_Planes]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_Classes] FOREIGN KEY([class_id])
REFERENCES [dbo].[Classes] ([id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_Classes]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_Flyghts] FOREIGN KEY([flyght_id])
REFERENCES [dbo].[Flyghts] ([id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_Flyghts]
GO
ALTER TABLE [dbo].[Tickets]  WITH CHECK ADD  CONSTRAINT [FK_Tickets_Passangers] FOREIGN KEY([passanger_id])
REFERENCES [dbo].[Passangers] ([id])
GO
ALTER TABLE [dbo].[Tickets] CHECK CONSTRAINT [FK_Tickets_Passangers]
GO
USE [master]
GO
ALTER DATABASE [airport] SET  READ_WRITE 
GO
