use [homework11]
go
/*1. ������� �������� � ����������� �����, �������������
� 5-� �������, ������������ 5 � ����� ����, ���� � ����
������� ���� ���� �� ���� ������ ������������ ����� 15 ����.*/

update Wards set Places = 17 where id = 6

-- ������� �������� � ����������� �����
select w1.Name, w1.Places 
from  Wards w1, Departments d1
where d1.Building = 5 -- ������������� � 5-� �������
  and w1.Places >= 5 -- ������������ 5 � ����� ����
  and w1.DeparmentsId = d1.Id
  and exists( -- ���� ����
	select max(w2.Places) from Wards w2, Departments d2
		where w1.DeparmentsId = d2.Id 
		  and d2.Building = d1.Building -- � ���� �������
		group by d2.Building
		having max(w2.Places) > 15 -- ���� �� ���� ������ ������������ ����� 15 ����
  )

/*
2. ������� �������� ��������� � ������� �����������
���� �� ���� ������������ �� ��������� ������.
*/
select Name from Departments d1
where exists( 
  select * from Wards w1, DoctorsExaminations de1
  where  d1.Id = w1.DeparmentsId
    and w1.Id = de1.WardId
    and datediff(day, de1.Date, getdate()) <= 7
)

/* 3. ������� �������� �����������, ��� ������� �� ���������� ������������. */
select Name from Diseases
where not exists(
	select * 
	from DoctorsExaminations
	where Diseases.Id = DoctorsExaminations.DiseaseId
)

/* 4. ������� ������ ����� ������, ������� �� �������� ������������. */
select Name + ' ' + Surname from Doctors
where not exists(
	select * 
	from DoctorsExaminations 
	where DoctorsExaminations.DoctorId=Doctors.Id
)

/* 5. ������� �������� ���������, � ������� �� ���������� ������������.*/
select Name from Departments d
where not exists(
  select * from Wards w, DoctorsExaminations de
	where d.Id = w.DeparmentsId and w.Id = de.WardId
)

/* 6. ������� ������� ������, ������� �������� ���������. */
select Surname from Doctors
where exists(
	select * from Inters 
	where Doctors.Id=Inters.DoctorId
)

/* 7. ������� ������� ��������, ������ ������� ������, ��� ������ ���� �� ������ �� ������. */
select Surname from Doctors, Inters
where Doctors.Id=Inters.DoctorId
 and  Salary > any(
		select Salary from Doctors
		where not exists(
			select * from Inters 
			where Doctors.Id=Inters.DoctorId
		)
	)

/* 8. ������� �������� �����, ��� ����������� ������, ���
����������� ������ ������, ����������� � 3-� �������. */
select Name from wards
where Places > all(
	select Places from wards, Departments
	where Wards.DeparmentsId = Departments.Id
      and Departments.Building = 3
)

/* 9. ������� ������� ������, ���������� ������������ �
���������� �Ophthalmology� � �Physiotherapy�. */
select Surname from Doctors doc
where exists (
	select * from DoctorsExaminations de, Wards w, Departments d
	  where (doc.Id = de.DoctorId 
	    and de.WardId = w.Id 
		and w.DeparmentsId = d.Id)
		and d.Name in ('Ophthalmology', 'Physiotherapy')
)

/* 10. ������� �������� ���������, � ������� �������� ������� � ����������. */
select d.Name 
from Departments d, Wards w, DoctorsExaminations de
where d.Id = w.DeparmentsId 
  and w.Id = de.WardId
  and de.DoctorId = any(
		select Id from Doctors 
		where exists (
			select * from Inters
				where Doctors.Id = Inters.DoctorId
		)
		union
		select Id from Doctors
		where exists (
			select * from Professors
				where Doctors.Id = Professors.DoctorId
		)
)

/* 11. ������� ������ ����� ������ � ��������� � ������� ��� �������� ������������, 
���� ��������� ����� ���� �������������� ����� 20000. */
select doc.Name + ' ' + doc.Surname, d.Name
from Departments d, Wards w, DoctorsExaminations de, Doctors doc
where d.Id = w.DeparmentsId 
  and w.Id = de.WardId
  and doc.Id = de.DoctorId
  and d.Financing > 20000

/* 12. ������� �������� ���������, � ������� �������� ������������ ���� 
   � ���������� �������. */
select d.Name
from Departments d, Wards w, DoctorsExaminations de, Doctors doc
where d.Id = w.DeparmentsId 
  and w.Id = de.WardId
  and doc.Id = de.DoctorId
  and doc.Salary = (select max(Salary) from Doctors)

/* 13. ������� �������� ����������� � ���������� ���������� �� ��� ������������. */
select Diseases.Name, count(*) from Diseases, DoctorsExaminations
where Diseases.Id = DoctorsExaminations.DiseaseId
group by Diseases.Name