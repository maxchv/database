create database homework11;
go
use homework11
go

create table Departments  ----- ���������
(
Id int IDENTITY PRIMARY KEY,
Building int NOT NULL check(Building BETWEEN 1 and 5),
Financing money NOT NULL check(Financing !<0) DEFAULT 0,
Name nvarchar(100) NOT NULL check(Name != '') unique
)
go
create table Wards ----- ������
(
Id int IDENTITY PRIMARY KEY,
Name nvarchar(20) NOT NULL check(Name != '') unique,
Places int NOT NULL check(Places !< 1),
DeparmentsId int NOT NULL references Departments (Id)
)
go
create table Diseases --- �����������
(
Id int IDENTITY PRIMARY KEY,
Name nvarchar(100) NOT NULL check(Name != '') unique
)
go
create table Examinations ---������������
(
Id int IDENTITY PRIMARY KEY,
Name nvarchar(100) NOT NULL check(Name != '') unique
)
go
create table Doctors ---�������
(
Id int IDENTITY PRIMARY KEY,
Name nvarchar(max) NOT NULL check(Name != ''),
Salary money NOT NULL check(Salary !< 0),
Surname nvarchar(max) NOT NULL check(Surname != '')
)
go
create table Inters ----- �������
(
Id int IDENTITY PRIMARY KEY,
DoctorId int NOT NULL references Doctors(Id)
)
go
create table Professors ---- ����������
(
Id int IDENTITY PRIMARY KEY,
DoctorId int NOT NULL references Doctors(Id)
)
go
create table DoctorsExaminations --- ����� � ������������
(
Id int IDENTITY PRIMARY KEY,
Date date NOT NULL check(Date !> getdate()) DEFAULT getdate(),
DiseaseId int NOT NULL references Diseases(Id),
DoctorId int NOT NULL references Doctors(Id),
ExaminationId int NOT NULL references Examinations (Id),
WardId int NOT NULL references Wards(Id)
)
go

insert into Departments(Building, Financing, [Name])
	values (3, 10000, N'Ophthalmology'),
	       (1, 20000, N'��� ���������'),
		   (3, 30000, N'Physiotherapy'),
		   (2, 40000, N'���������������� ���������'),
		   (3, 50000, N'��������������� ���������'),
		   (5, 50000, N'������������ ���������'),
		   (5, 50000, N'������� ���������')
go

insert into Diseases([Name])
	values (N'�����'),
	       (N'�����'),
		   (N'�������'),
		   (N'������'),
		   (N'�������'),
		   (N'����'),
		   (N'COVID-19'),
		   (N'����')
go

insert into Doctors([Name], Salary, Surname)
	values (N'����', 2000, N'������'),
	       (N'�������', 4000, N'���������'),
		   (N'�������', 8000, N'�������'),
		   (N'������', 106000, N'������'),
		   (N'����', 24000, N'�����'),
		   (N'������', 15000, N'�������'),
		   (N'�����', 50000, N'������')
go

insert into Examinations([Name])
	values (N'Endoscopy'),
	       (N'Biopsy'),
		   (N'Bronchoscopy'),
		   (N'Autopsy'),
		   (N'Cardiac Catheterization')
		   
go

insert into Wards(Name, Places, DeparmentsId)
	values (N'������ �1', 6, 1),
	       (N'������ �2', 5, 2),
		   (N'������ �3', 8, 3),
		   (N'������ �4', 10, 4),
		   (N'������ �5', 16, 5),
		   (N'������ �5.1', 17, 6),
		   (N'������ �5.2', 15, 6),
		   (N'�������', 6, 7)
go

insert into DoctorsExaminations(Date, DiseaseId, DoctorId, ExaminationId, WardId)
	values ('2021-03-10', 1, 1, 1, 1),
	       ('2021-02-22', 2, 2, 2, 2),
		   ('2021-01-26', 3, 3, 3, 3),
		   (getdate(), 4, 4, 4, 4),
		   (getdate(), 5, 5, 5, 5)
go

insert into Inters(DoctorId)
	values (1),
	       (6)
go

insert into Professors(DoctorId)
	values (3),	
		   (4),	
		   (5)
go