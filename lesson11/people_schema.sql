drop database if exists People;
go
-- создаем базу данных 
create database People;
go
use People;
go
create table People
(
    id int identity primary key,
    name nvarchar(50) not null,
    age int not null,
    weight int not null
) ;
go
insert into People(name, age, weight)
    values('Harry', 34, 80),
          ('Sally', 28, 64),
          ('George', 29, 70),
          ('Helena', 54, 54),
          ('Peter', 34, 80);
go

create table Characters
(
    id int identity primary key,
    name nvarchar(50) not null,
    age int not null,
    weight int not null
) ;
go
insert into Characters(name, age, weight)
    values('Daffy', 24, 19), 
          ('Donald', 25, 23),
          ('Scrooge', 81, 27),
          ('George', 29, 70),
          ('Sally', 28,	64);
