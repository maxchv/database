drop database if exists Cartoons;
go
create database Cartoons;
go
use Cartoons;
go
create table Cartoons
(
    id_cartoon      int identity primary key,
    name_cartoon    nvarchar(100) not null,
	channel_id      int
) 
go

insert into Cartoons(name_cartoon)
    values('The Simpsons'),
          ('Family Guy'),
          ('Duck Tales');
go
create table Channels
(
    id_channel      int identity primary key,
    name_channel    nvarchar(100) not null
) 
go

insert into Channels(name_channel)
    values('1+1'),
          ('СТБ'),
          ('Интер'),
          ('2+2'),
          ('1+1'),
          ('BBC');


insert into Cartoons(name_cartoon, channel_id)
    values('The Simpsons', 1),
          ('Family Guy', 1),
          ('Duck Tales', 2);

insert into Cartoons(name_cartoon, channel_id)
	values('Futurama', 10),
          ('Spanch Bob', 15);







