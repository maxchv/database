-- SQL: DDL (CREATE, DROP, ALTER), DML (INSERT, UPDATE, DELETE, SELECT)

-- 1 �������������
/*
������������� - ��� ���������� ������� ��� ���������� ���������� �� ������ ������.
������������� -  ����������� ������

�����?
	- ������������
	- �������� ��������
	- ������ �� ���������
	- ����������� ������
*/
go
-- �������� �������������
/*
CREATE VIEW <���_�������������>[(<����1>, <����2�>,...)] 
    AS
    SELECT ...
*/
go
-- ������� �������������
create view simple_view
as
select * from table1
go
insert into table1 values (2,3),(4,5), (6,7)

go
create  view only_one_field
as
select f1 from table1
alter table table1
alter column f2 int
go
select * from only_one_field
insert into only_one_field values(200)
go
select * from simple_view
select * from (select * from table1) as t
insert into simple_view values (2,3),(4,5), (6,7)
go
create view EmployeeOrdersView
as
select FirstName, LastName, Count(o.EmployeeID) Orders
	from Employees e, Orders o
	where e.EmployeeID = o.EmployeeID
	group by FirstName, LastName
go
select * from EmployeeOrdersView order by Orders desc
go
-- �������
/*
������������ ���� ������ Northwind

1. ������� �������������, ������� ���������� CompanyName, ContactTitle � 
ContactName �� ������� Customers, ������� ����� ���������� ���������� 
������� (�� ������� Orders)

2. �������� ������������� ������� �� ���������� ���������� ��������� � 
������ ������ (city). ������� Employees
*/

select CustomerID, CompanyName, ContactName, ContactTitle from Customers
go
create view CustomersOrdersCountView
as
SELECT CustomerID, count(OrderID) OrdersCount
		FROM Orders 
		GROUP BY CustomerID
go
select * from CustomersOrdersCountView
go
-- �������������
SELECT CompanyName, ContactName, ContactTitle 
FROM Customers
WHERE CustomerID in (SELECT CustomerID 
FROM CustomersOrdersCountView oc
WHERE oc.OrdersCount = (SELECT max(OrdersCount) FROM CustomersOrdersCountView))
go
SELECT * from Customers where CustomerID in ('ALFKI', 'AROUT')
go
---------------------------------------------------------------
go
-- 2 ��������� EXISTS, NOT EXISTS, ANY/SOME
go
-- 2.1 EXISTS
/*
���� ��������� ��������
���� �� ���� ������, �� �������� EXISTS ���������� ��-
���� � ������� ������ ���������� � ��������������
�������, ����� �������� ���������� ���� �, �������-
�������, ������ �� ������������.
*/
-- �������� ��� �������� ��� ������� ���� ���������� EXISTS
go
-- �������� ��� �������� ��� ������� ��� ����������� NOT EXISTS
go
-- 2.2 ���� �� ���� SOME/ANY
/*
��������� ANY � SOME �������� ���������� �
������������ �������� ���������� ��������� �������
��������� ���� �� ��� ������ �������� �� ���, �������
���������� ���������.
*/
go
-- ����� �����������, ������� ��������� ���� �� ���� ������

go
-- 2.3 ��� ALL
/*
�������� ALL ������������ ��� ��������� ������-
����� ����������, ����� �������, ����� ����������
������� ������������� ��� ���������� ���������� ���
����������.
*/
go
--------------------------------------------------------------------
go
-- 3 ����������� �������
/*
����������� ������� ���������� �� ������ �������� 
� �������� ������� ������ ������ ��� ������.
*/
go
-- �������� ������ �������� ����������� �������:
/*
������������ �������� ��� �����������:

    �����������            UNION
    �����������            INTERSECT
    ���������              EXCEPT
    ��������� ������������ CROSS JOIN ,

����������� ����������� ��������:

    �������                SELECT DISTINCT * FROM ... WHERE ...
    ��������               SELECT DISTINCT col1, col2 FROM ... 
    ����������             JOIN
    �������

����������� ����� �������� ������� ��� ����������� �������� ��� ���� ���������, 
������� ����� ����� ��� �� ������������ � ������ ���������.
*/
go
-- 3.1 ��������
/*
�������� �������� ���������, ��� ������� �� ��������� ���������� �������� ������ 
�� ��������� �������, �� ���� �� ������� ���������� ������ ������ �������, 
��� ����, ���� ��������� ��������� ���������� ��������, �� � �������������� 
��������� �������� ������ �� ������ ���������� ��������� �������.
*/
go 
-- 3.2 �������
/*
������� � ��� ��������, ������� �������� ��������� ����� � �������, 
��������������� �������� ��������.
*/
go
-- �������� � �������
/*
��������� ��������� �������� � �������. �� ����� ��� �������, ������ ��� ����� �� ���������� � ���������� ���������� ��������� � � 
�������� ���������� ���������� ����� ���������.
*/
go
-- 3.3 ����������� UNION
/*
https://intellect.icu/th/25/blogs/id4462/3_pictorial-representation-of-union.png
����������� ����������� ��������� A � B ����� ��������� � ��� �� ����������, ��� � � ����������� �� ���� ��������� A � B, � �����, ��������� �� ��������, 
������������� ��� A, ��� B, ��� ����� ����������.
*/

go
-- �������
/*
	� ���� ������ Northwind
	����� ��� ������ � ������� ��������� ���������� (suppliers) 
	� ��������� (customers) 
*/
go

-- 3.4 ����������� INTERSECT
-- ����� ���� ����������� � ������� Employees

go
-- 3.5 ��������� EXCEPT
-- �������� ���� �����������

go
-- 3.6 ���������� ������������ CROSS JOIN

go
-- 3.7 ���������� JOIN
-- http://www.dtulyakov.ru/images/SQL_Joins.svg
go
-- 3.7.1 ���������� ���������� INNER JOIN
go
-- ������� �������� ��������� � �������� ������������ 
-- ��� �������� 
select ProductName, CompanyName from Products, Suppliers
where Suppliers.SupplierID = Products.SupplierID
--
select ProductName, CompanyName 
from Products cross join Suppliers
where Suppliers.SupplierID = Products.SupplierID
--
select ProductName, CompanyName 
from Products
join Suppliers
on Suppliers.SupplierID = Products.SupplierID
go
-- ������� ���������� OUTER JOIN
go
-- 3.7.2 ����� ���������� LEFT OUTER JOIN
select * from Products
-- insert into Suppliers(CompanyName, ContactName, ContactTitle, Address, City, Country)
-- values (N'���� � ������', N'���� ������', N'����� ����� �������', N'������� �������, 102', N'������', N'�������')
select CompanyName, ProductName from Suppliers
left outer join Products
on Suppliers.SupplierID = Products.SupplierID

go

-- 3.7.3 ������ ������� ���������� RIGHT OUTER JOIN
-- insert into Products(ProductName, UnitPrice, UnitsInStock, UnitsOnOrder)
-- values (N'�������', 1000, 10, 0)
select CompanyName, ProductName 
from Suppliers
right outer join Products
on Suppliers.SupplierID = Products.SupplierID
go

-- 3.7.4 ������ ������� ���������� FULL JOIN
select CompanyName, ProductName 
from Suppliers
full outer join Products
on Suppliers.SupplierID = Products.SupplierID
go



	
