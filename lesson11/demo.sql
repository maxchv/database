-- �������������
/*
������������� - ��� ���������� ������� ��� ���������� ���������� �� ������ ������.

�����?
	- ������������
	- �������� ��������
	- ������ �� ���������
	- ����������� ������
*/

-- Customers.Name + Employees.Name
-- �����������
select ContactName Name from Customers
union
select FirstName + ' ' + LastName Name from Employees
order by Name

-- �����������
-- ����������
select FirstName, LastName from Employees
intersect
select -- e.EmployeeID, e.FirstName, e.LastName, e.ReportsTo, 
	   r.FirstName, r.LastName
	from Employees e, Employees r
	where e.ReportsTo = r.EmployeeID

-- ���������
-- �������� ���� �����������
select FirstName, LastName from Employees
except
select r.FirstName, r.LastName
	from Employees e, Employees r
	where e.ReportsTo = r.EmployeeID

-- ���������� ������������
select * from Products
cross join 
Suppliers

-- ����������
select ProductName, CompanyName [Supplier Company] 
	from Products p, Suppliers s
	where p.SupplierID = s.SupplierID
-- ���������� ����������
select ProductName, CompanyName [Supplier Company] 
	from Products p
	inner join 
	Suppliers s
	on p.SupplierID = s.SupplierID
-- ������� ����������
select * from Products
insert into Suppliers(CompanyName, ContactName, ContactTitle, Address, City, Country)
values (N'���� � ������', N'���� ������', N'����� ����� �������', N'������� �������, 102', N'������', N'�������')
go
-- ����� ����������
select CompanyName [Supplier Company], ProductName
	from 
	Suppliers s
	left outer join
	Products p
	on s.SupplierID = p.SupplierID
insert into Products(ProductName, UnitPrice, UnitsInStock, UnitsOnOrder)
values (N'�������', 1000, 10, 0)

-- ������ ������� ����������
select CompanyName [Supplier Company], ProductName
	from 
	Suppliers s
	right outer join
	Products p
	on s.SupplierID = p.SupplierID

-- ������ ������� ����������
select CompanyName [Supplier Company], ProductName
	from 
	Suppliers s
	full join
	Products p
	on s.SupplierID = p.SupplierID

-- EXISTS, NOT EXISTS, ANY/SOME
-- �������� ��� �������� ��� ������� ���� ����������
select ProductName from Products p
	where EXISTS (select * from Suppliers s where p.SupplierID = s.SupplierID)
-- �������� ��� �������� ��� ������� ��� �����������
select ProductName from Products p
	where NOT EXISTS (select * from Suppliers s where p.SupplierID = s.SupplierID)

-- 
insert into Employees(FirstName, LastName) values(N'����', N'������')

-- SOME/ANY
select FirstName, LastName from Employees e
	where e.EmployeeID = any (select o.EmployeeID from Orders o)

-- ALL
select FirstName, LastName from Employees e
	where e.EmployeeID != all (select o.EmployeeID from Orders o)
	
