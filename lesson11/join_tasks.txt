Используя бд из cartoons_schema.sql

1. Соеденить данные из таблиц cartoons и channels использяу inner join.
   Вывести id_cartoons, name_cartoon, channel_id, id_channel

2. Используя левое внешнее соединение (left outer join) соединить данные
   таблиц cartoons и channels. 
   Вывести id_cartoons, name_cartoon, channel_id, id_channel
   Сравнить с выводом при внутреннем соединении

3. Используя правое внешнее соединение (right outer join) соединить данные
   таблиц cartoons и channels. 
   Вывести id_cartoons, name_cartoon, channel_id, id_channel
   Сравнить с выводом при внутреннем и внешнем левым соединении 

4. Сделать полное внешнее соединение таблиц cartoons и channels.
   Сравнить с предыдущими результатами
    