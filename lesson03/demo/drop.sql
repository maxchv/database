if db_id('hellodb') is not null
begin
	drop database hellodb
end
-- since MS SQL 2016
drop database if exists db
go
use filippov
go
if object_id('students') is not null
begin
	drop table students
end
-- since MS SQL 2016
drop table if exists students -- filippov.dbo.students
go
