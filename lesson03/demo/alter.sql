drop database if exists danyadb
go
-- before MS SQL v 2016
if db_id('demo') is not null
begin
	drop database demo
end

-- ALTER DEMO
use Hospital_Ruslan_db
go
alter table doctor
	add first_name nvarchar(50) not null
go
alter table doctor
	add last_name nvarchar(50) not null
go
alter table doctor
	alter column first_name nvarchar(100) null
go
alter table doctor
	drop column first_name
go
/*create table doctor
(
	...
	unique(first_name, last_name)
)*/
alter table doctor
	add constraint fn_ln_unique
		unique(first_name, last_name)
go

alter table doctor
	add constraint cabinet_id_default_1
		default 1 for cabinetid
go

---
create database demo
go
use demo
go
create table students
(
	id int primary key identity,
	first_name nvarchar(50) not null,
	last_name nvarchar(50) not null
)
go
create table [social network]
(
	id int primary key identity,
	facebook nvarchar(50) not null
)
go
alter table [social network]
	add student_id int
go
alter table [social network]
	add constraint social_network_students_fk
		foreign key(student_id) references students(id)
go
alter table [social network]
	add constraint students_id_unique
		unique(student_id)
go