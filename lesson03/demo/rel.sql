drop database if exists demo
go
create database demo
go
use demo
go
create table students
(
	id int primary key identity,
	first_name nvarchar(50) not null,
	last_name nvarchar(50) not null
)
go
create table [social networks]
(
	id int primary key identity,
	facebook varchar(100),
	instagram varchar(100)
)
go
-- ��������� ������� � ������� ����� ����������� ������� ��
-- ��������� ���� ������� students
alter table [social networks]
add student_fk int
go
-- ��������� ����������� �� ������� ����
alter table [social networks]
add constraint student_fk_constraint
foreign key(student_fk) references students(id)
go
-- ��������� ����������� �� ������������ ������� �����������
-- ��������� ����� ��������� ���� ���� � ������
alter table [social networks]
add constraint student_fk_unique
unique(student_fk)
go

alter table [social networks]
drop constraint student_fk_unique, student_fk_constraint
go

alter table [social networks]
drop column student_fk
go

alter table [social networks]
add constraint student_fk_constraint2
foreign key(id) references students(id)
go