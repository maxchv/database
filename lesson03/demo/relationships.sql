create database academy2
go
use academy2
go
create table students
(
	id int primary key identity,
	first_name nvarchar(50) not null,
	last_name nvarchar(50) not null
	--, groups_fk int not null default 1 foreign key(groups_fk) references groups(id)
)
go
create table groups
(
	id int primary key identity,
	name nvarchar(50) not null unique
)
go
alter table students
add groups_fk int  null
go
alter table students
add constraint student_groups_fk 
foreign key(groups_fk) references groups(id) 
on delete set default
-- set null, cascade, no action
on update cascade
go
alter table students
drop constraint student_groups_fk
go
alter table students
alter column groups_fk int not null
go
alter table students
add constraint default_group_fk default 1 for groups_fk
go
create table [social networks]
(
	id int primary key identity,
	facebook varchar(100),
	instagram varchar(100),
	foreign key(id) references students(id)
	-- student_fk int foreign key (student_fk) references students(id) unique
)




