print('start')
-- �������� �� ������� �� � ������ db � �������� � ������ �������
-- ����� if exists ���������� � MS SQL ������� � 2016 ������
select db_id('mall')
drop database if exists mall
go
create database mall
go
use mall
go
create table customers
(
	id int primary key identity,
	first_name varchar(30) not null check(first_name != '')
)
go
-- ��������� �������
alter table customers
add last_name varchar(30) not null
go
-- ������� �������
alter table customers
drop column last_name
go
-- �������������� ����������� �������
alter table customers
alter column first_name varchar(50)
print('end')
